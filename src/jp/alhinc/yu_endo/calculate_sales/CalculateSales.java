package jp.alhinc.yu_endo.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {

		// コマンドライン引数存在確認
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		// 変数定義
		Map<String, String> branches = new HashMap<>();
		Map<String, String> commodities = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();
		Map<String, Long> commoditySales = new HashMap<>();

		// 支店定義ファイル存在確認
		String directory = args[0];
		String branchList = "branch.lst";

		if (!checkBranchList(directory, branchList)) {
			return;
		}

		// 支店定義ファイル読み込み
		String branchIdFormat = "^[0-9]{3}$";

		if (!inputBranchList(directory, branchList, branchIdFormat, branches, branchSales)) {
			return;
		}

		// 商品定義ファイル存在確認
		String commodityList = "commodity.lst";

		if (!checkCommodityList(directory, commodityList)) {
			return;
		}

		// 商品定義ファイル読み込み
		String commodityIdFormat = "^[a-zA-Z0-9]{8}$";

		if (!inputCommodityList(directory, commodityList, commodityIdFormat, commodities, commoditySales)) {
			return;
		}

		// 売上ファイル読み込み
		String salesFileFormat = "^[0-9]{8}\\.rcd";

		List<File> salesFiles = inputSales(directory, salesFileFormat);

		// 売上ファイル名連番確認
		if (!checkSalesFiles(salesFiles)) {
			return;
		}

		// 売上ファイルから抽出、集計
		if (!calculateSales(salesFiles, branches, commodities, branchSales, commoditySales)) {
			return;
		}

		// 支店別集計ファイル出力
		String branchOut = "branch.out";

		if (!outputBranchSales(directory, branchOut, branches, branchSales)) {

			System.out.println("予期せぬエラーが発生しました");
			return;

		}

		// 商品別集計ファイル出力
		String commodityOut = "commodity.out";

		if (!outputCommoditySales(directory, commodityOut, commodities, commoditySales)) {

			System.out.println("予期せぬエラーが発生しました");
			return;

		}
	}




	// 支店定義ファイル存在確認メソッド
	static boolean checkBranchList(String directory, String branchList) {

		File branchListFile = new File(directory, branchList);

		if (!branchListFile.exists()) {

			System.out.println("支店定義ファイルが存在しません");
			return false;

		}
		return true;
	}



	// 支店ファイル読み込みメソッド
	static boolean inputBranchList(String directory, String branchList, String branchIdFormat, Map<String, String> branches, Map<String, Long> branchSales) {

		File file = new File(directory, branchList);
		BufferedReader brBranchesFile = null;

		try {
			FileReader frFile = new FileReader(file);
			brBranchesFile = new BufferedReader(frFile);

			String line;

			while ((line = brBranchesFile.readLine()) != null) {

				String[] branchInfo = line.split(",");

				if ((branchInfo.length != 2) || !branchInfo[0].matches(branchIdFormat)) {

					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;

				}

				String branchId = branchInfo[0];
				String branchName = branchInfo[1];
				Long branchTotalSales = 0L;

				branches.put(branchId, branchName);
				branchSales.put(branchId, branchTotalSales);

			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (brBranchesFile != null) {
				try {
					brBranchesFile.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}




	// 商品定義ファイル存在確認メソッド
	static boolean checkCommodityList(String directory, String commodityList) {

		File commodityListFile = new File(directory, commodityList);

		if (!commodityListFile.exists()) {

			System.out.println("商品定義ファイルが存在しません");
			return false;

		}
		return true;
	}




	// 商品定義ファイル読み込みメソッド
	static boolean inputCommodityList(String directory, String commodityList, String commodityIdFormat, Map<String, String> commodities, Map<String, Long> commoditySales) {

		File file = new File(directory, commodityList);
		BufferedReader brCommoditiesFile = null;

		try {
			FileReader frFile = new FileReader(file);
			brCommoditiesFile = new BufferedReader(frFile);

			String line;

			while ((line = brCommoditiesFile.readLine()) != null) {

				String[] commodityInfo = line.split(",");

				if ((commodityInfo.length != 2) || !commodityInfo[0].matches(commodityIdFormat)) {

					System.out.println("商品定義ファイルのフォーマットが不正です");
					return false;

				}

				String commodityId = commodityInfo[0];
				String commodityName = commodityInfo[1];
				Long commodityTotalSales =0L;

				commodities.put(commodityId, commodityName);
				commoditySales.put(commodityId, commodityTotalSales);

			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (brCommoditiesFile != null) {
				try {
					brCommoditiesFile.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}




	// 売上ファイル読み込みメソッド
	static List<File> inputSales(String directory, String salesFileFormat) {

		File dir = new File(directory);
		File[] list = dir.listFiles();
		List<File> salesFiles = new ArrayList<>();

		for (File file : list) {

			if (file.isFile() && file.getName().matches(salesFileFormat)) {

				salesFiles.add(file);

			}
		}
		return salesFiles;
	}




	// 売上ファイル連番確認メソッド
	static boolean checkSalesFiles(List<File> salesFiles) {

		Collections.sort(salesFiles);

		for (int i = 0; i < salesFiles.size() -1; i++) {

			String fileNumber = salesFiles.get(i).getName().substring(0, 8);
			String nextFileNumber = salesFiles.get(i + 1).getName().substring(0, 8);

			if ((Integer.parseInt(nextFileNumber) - Integer.parseInt(fileNumber)) != 1) {

				System.out.println("売上ファイル名が連番になっていません");
				return false;

			}
		}
		return true;
	}




	// 売上抽出・集計
	static boolean calculateSales(List<File> salesFiles, Map<String, String> branches, Map<String, String> commodities, Map<String, Long> branchSales, Map<String, Long> commoditySales) {


		for (File salesFile : salesFiles) {

			String fileName = salesFile.getName();
			BufferedReader brSalesFile = null;

			try {
				FileReader fr = new FileReader(salesFile);
				brSalesFile = new BufferedReader(fr);

				String salesLine;
				List<String> salesInfo = new ArrayList<>();

				while ((salesLine = brSalesFile.readLine()) != null) {

					salesInfo.add(salesLine);

				}

				if (salesInfo.size() != 3) {

					System.out.printf("%sのフォーマットが不正です", fileName);
					return false;

				}

				String branchId = salesInfo.get(0);

				if (!branches.containsKey(branchId)) {

					System.out.printf("%sの支店コードが不正です", fileName);
					return false;

				}

				String commodityId = salesInfo.get(1);

				if (!commodities.containsKey(commodityId)) {

					System.out.printf("%sの商品コードが不正です", fileName);
					return false;

				}

				String sales = salesInfo.get(2);

				if (!sales.matches("^[0-9]+$")) {

					System.out.println("予期せぬエラーが発生しました");
					return false;

				}

				long branchTotalSales = branchSales.get(branchId) + Long.parseLong(sales);
				long commodityTotalSales = commoditySales.get(commodityId) + Long.parseLong(sales);

				if ((branchTotalSales >= 10000000000L) || (commodityTotalSales >= 10000000000L)) {

					System.out.println("合計金額が10桁を超えました");
					return false;

				}

				branchSales.put(branchId, branchTotalSales);
				commoditySales.put(commodityId, commodityTotalSales);

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			} finally {
				if (brSalesFile != null) {
					try {
						brSalesFile.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return false;
					}
				}
			}
		}
		return true;
	}




	// 支店別集計ファイル出力メソッド
	static boolean outputBranchSales(String directory, String branchOut, Map<String, String> branches, Map<String, Long> branchSales) {

		File file = new File(directory, branchOut);
		BufferedWriter bwBranchOut = null;

		try {
			FileWriter fwFile = new FileWriter(file);
			bwBranchOut = new BufferedWriter(fwFile);

			for (String key : branches.keySet()) {

				String branchId = key;
				String branchName = branches.get(key);
				Long branchTotalSales = branchSales.get(key);

				bwBranchOut.write(branchId + "," + branchName + "," + branchTotalSales);
				bwBranchOut.newLine();
			}
		} catch (IOException e) {
			return false;
		} finally {
			if (bwBranchOut != null) {
				try {
					bwBranchOut.close();
				} catch (IOException e) {
					return false;
				}
			}
		}
		return true;
	}




	// 商品別集計ファイル出力メソッド
	static boolean outputCommoditySales(String directory, String commodityOut, Map<String, String> commodities, Map<String, Long> commoditySales) {

		File file = new File(directory, commodityOut);
		BufferedWriter bwCommodityOut = null;

		try {
			FileWriter fwFile = new FileWriter(file);
			bwCommodityOut = new BufferedWriter(fwFile);

			for (String key : commodities.keySet()) {

				String commodityId = key;
				String commodityName = commodities.get(commodityId);
				Long commodityTotalSales = commoditySales.get(commodityId);

				bwCommodityOut.write(commodityId + "," + commodityName + "," + commodityTotalSales);
				bwCommodityOut.newLine();

			}
		} catch (IOException e) {
			return false;
		} finally {
			if (bwCommodityOut != null) {
				try {
					bwCommodityOut.close();
				} catch (IOException e) {
					return false;
				}
			}
		}
		return true;
	}
}